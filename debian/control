Source: frotz
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Stephen Kitt <skitt@debian.org>
Homepage: https://davidgriffith.gitlab.io/frotz/
Build-Depends:
 debhelper-compat (= 13),
 libao-dev,
 libfreetype-dev,
 libjpeg-dev,
 libmodplug-dev,
 libncurses-dev,
 libpng-dev,
 libsamplerate0-dev,
 libsdl2-dev,
 libsdl2-mixer-dev,
 libsndfile1-dev,
 libvorbis-dev,
 zlib1g-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/games-team/frotz
Vcs-Git: https://salsa.debian.org/games-team/frotz.git
Rules-Requires-Root: no

Package: frotz
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 zcode-interpreter
Conflicts:
 zcode-support
Replaces:
 zcode-support
Description: interpreter of Z-code story-files
 Frotz interprets Z-code story-files, which are usually text adventure
 games (although a few arcade-style Z-code games have been written).
 Examples of such story files include the adventure games published by
 Infocom, as well as any games produced by compilers to this format,
 such as Inform.  You can find a number of Inform-compiled games up for
 anonymous FTP at ftp://ftp.ifarchive.org/ or HTTP at http://www.ifarchive.org/
 .
 Frotz complies with the Z Machine specification version 1.0.
 .
 This package contains the dumb and Curses versions of Frotz.

Package: sdlfrotz
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 zcode-interpreter
Conflicts:
 zcode-support
Replaces:
 zcode-support
Description: interpreter of Z-code story-files (SDL version)
 Frotz interprets Z-code story-files, which are usually text adventure
 games (although a few arcade-style Z-code games have been written).
 Examples of such story files include the adventure games published by
 Infocom, as well as any games produced by compilers to this format,
 such as Inform.  You can find a number of Inform-compiled games up for
 anonymous FTP at ftp://ftp.ifarchive.org/ or HTTP at http://www.ifarchive.org/
 .
 Frotz complies with the Z Machine specification version 1.0.
 .
 This package contains the SDL version of Frotz, which adds support
 for graphical Z6 games.
